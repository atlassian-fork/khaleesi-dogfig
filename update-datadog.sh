dogfig_location=$(which dogfig)
echo "Attempting to update dashboards with: ${dogfig_location}"

until dogfig --overwrite
do
   echo "Trying again";
   sleep 1;
done