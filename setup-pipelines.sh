#!/usr/bin/env bash

pip install dogfig --extra-index-url https://atlassian.jfrog.io/atlassian/api/pypi/conf-sre-pypi-local/simple

dogfig_location=$(which dogfig)
echo "Dogfig installed at: ${dogfig_location}"